package com.example.covid19stats.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CovidStats {

    @SerializedName("Global")
    @Expose
    private GlobalCovidStats globalCovidStats;
    @SerializedName("Countries")
    @Expose
    private List<CountriesCovidStats> countriesCovidStats;

    public GlobalCovidStats getGlobalCovidStats() {
        return globalCovidStats;
    }

    public void setGlobalCovidStats(GlobalCovidStats globalCovidStats) {
        this.globalCovidStats = globalCovidStats;
    }

    public List<CountriesCovidStats> getCountriesCovidStats() {
        return countriesCovidStats;
    }

    public void setCountriesCovidStats(List<CountriesCovidStats> countriesCovidStats) {
        this.countriesCovidStats = countriesCovidStats;
    }
}