package com.example.covid19stats.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountriesCovidStats {


    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("TotalConfirmed")
    @Expose
    private int totalConfirmed = 0;
    @SerializedName("TotalDeaths")
    @Expose
    private int totalDeaths = 0;
    @SerializedName("TotalRecovered")
    @Expose
    private int totalRecovered = 0;


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }

    public void setTotalConfirmed(int totalConfirmed) {
        this.totalConfirmed = totalConfirmed;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public void setTotalDeaths(int totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public void setTotalRecovered(int totalRecovered) {
        this.totalRecovered = totalRecovered;
    }


}
