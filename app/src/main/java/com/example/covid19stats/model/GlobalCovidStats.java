package com.example.covid19stats.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GlobalCovidStats {

    @SerializedName("TotalConfirmed")
    @Expose
    private int globalTotalConfirmed;
    @SerializedName("TotalDeaths")
    @Expose
    private int globalTotalDeaths;
    @SerializedName("TotalRecovered")
    @Expose
    private int globalTotalReoovered;


    public int getGlobalTotalConfirmed() {
        return globalTotalConfirmed;
    }

    public void setGlobalTotalConfirmed(int globalTotalConfirmed) {
        this.globalTotalConfirmed = globalTotalConfirmed;
    }

    public int getGlobalTotalDeaths() {
        return globalTotalDeaths;
    }

    public void setGlobalTotalDeaths(int globalTotalDeaths) {
        this.globalTotalDeaths = globalTotalDeaths;
    }

    public int getGlobalTotalReoovered() {
        return globalTotalReoovered;
    }

    public void setGlobalTotalReoovered(int globalTotalReoovered) {
        this.globalTotalReoovered = globalTotalReoovered;
    }
}
