package com.example.covid19stats.resources;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;


public class ResourceManager {

    public static void printlog(String message) {
        Log.d("Print", message);
    }

    public static void printlog(String tag, String message) {
        Log.d(tag, message);
    }

    public static void printlog(String tag, String message, Throwable error) {
        Log.d(tag, message, error);
    }



   /* public static boolean haveNetworkConnection(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }*/


   /* public static boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

        if (activeNetworkInfo != null) { // connected to the internet
          *//*  Toast.makeText(context, activeNetworkInfo.getTypeName(), Toast.LENGTH_SHORT).show();*//*

            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        }
        return false;
    }*/

    public static AlertDialog.Builder getDialogProgressBar(Context context) {

        AlertDialog.Builder builder = null;

        if (builder == null) {
            builder = new AlertDialog.Builder(context);
            builder.setTitle("Loading...");

            final ProgressBar progressBar = new ProgressBar(context);
            progressBar.setIndeterminate(true);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            progressBar.setLayoutParams(lp);
            builder.setView(progressBar);
        }
        return builder;
    }

}
