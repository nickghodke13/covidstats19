package com.example.covid19stats.storage;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private SharedPreferences preference;
    private SharedPreferences.Editor editor;
    private Context context;

    public AppPreferences(Context context) {
        preference = context.getSharedPreferences("COVID-19", Context.MODE_PRIVATE);
        editor = preference.edit();
        this.context = context;
    }


    public boolean getFineLocationPermission() {
        return preference.getBoolean(Manifest.permission.ACCESS_FINE_LOCATION, false);
    }

    public void setFineLocationPermission(boolean fineLocationState) {
        editor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION, fineLocationState);
        editor.commit();
    }

   /* public boolean getCoarseLocationPermission() {
        return preference.getBoolean(Manifest.permission.ACCESS_COARSE_LOCATION, false);
    }

    public void setCoarseLocationPermission(boolean coarseLocationState) {
        editor.putBoolean(Manifest.permission.ACCESS_COARSE_LOCATION, coarseLocationState);
        editor.commit();
    }*/

    public String getLatitude() {

        return preference.getString("latitude", "");
    }

    public void setLatitude(String latitude) {

        editor.putString("latitude", latitude);
        editor.commit();

    }

    public String getLongitude() {

        return preference.getString("longitude", "");
    }

    public void setLongitude(String longitude) {

        editor.putString("longitude", longitude);
        editor.commit();

    }

}
