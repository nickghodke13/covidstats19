package com.example.covid19stats.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.covid19stats.R;
import com.example.covid19stats.model.CountriesCovidStats;

import java.util.ArrayList;
import java.util.List;

public class ListViewAdapter extends BaseAdapter implements Filterable {
    List<CountriesCovidStats> countriesCovidStatsList = new ArrayList<>();
    private List<CountriesCovidStats> countriesCovidStatsOriginalData = new ArrayList<>();
    Context context;
    private LayoutInflater inflater;
    private ItemFilter mFilter = new ItemFilter();

    public ListViewAdapter(Context context, List<CountriesCovidStats> countriesCovidStatsList) {
        this.context = context;
        this.countriesCovidStatsList = countriesCovidStatsList;
        this.countriesCovidStatsOriginalData = countriesCovidStatsList;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return countriesCovidStatsList.size();
    }

    @Override
    public CountriesCovidStats getItem(int position) {
        return countriesCovidStatsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {


        /*RowLayoutBinding binding = DataBindingUtil.getBinding(convertView);

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.row_layout, parent, false);
        }
        binding.setCountryCovidData(countriesCovidStatsList.get(position));
        binding.executePendingBindings();
        return binding.getRoot();*/
        ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();

            convertView = inflater.
                    inflate(R.layout.row_layout, parent, false);

            holder.txtCountryName = (TextView)
                    convertView.findViewById(R.id.txtCountryName);
            holder.txtTotalCases = (TextView)
                    convertView.findViewById(R.id.txtTotalCases);
            holder.txtTotalDeaths = (TextView)
                    convertView.findViewById(R.id.txtTotalDeaths);
            holder.txtTotalRecovered = (TextView)
                    convertView.findViewById(R.id.txtTotalRecovered);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // get current item to be displayed
        CountriesCovidStats countriesCovidStats = countriesCovidStatsList.get(position);

        // get the TextView for item name and item description
       /* TextView txtCountryName = (TextView)
                convertView.findViewById(R.id.txtCountryName);
        TextView txtTotalCases = (TextView)
                convertView.findViewById(R.id.txtTotalCases);
        TextView txtTotalDeaths = (TextView)
                convertView.findViewById(R.id.txtTotalDeaths);
        TextView txtTotalRecovered = (TextView)
                convertView.findViewById(R.id.txtTotalRecovered);*/

        //sets the text for item name and item description from the current item object
        holder.txtCountryName.setText(countriesCovidStats.getCountry());
        holder.txtTotalCases.setText(Integer.toString(countriesCovidStats.getTotalConfirmed()));
        holder.txtTotalDeaths.setText(Integer.toString(countriesCovidStats.getTotalDeaths()));
        holder.txtTotalRecovered.setText(Integer.toString(countriesCovidStats.getTotalRecovered()));

        // returns the view for the current row
        return convertView;

    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ViewHolder {

        protected TextView txtCountryName;
        protected TextView txtTotalCases;
        protected TextView txtTotalDeaths;
        protected TextView txtTotalRecovered;

    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase().replaceAll("\\s", "");

            FilterResults results = new FilterResults();

            final List<CountriesCovidStats> list = countriesCovidStatsOriginalData;

            int count = list.size();
            final ArrayList<CountriesCovidStats> nlist = new ArrayList<>(count);

            String country;
            int totalConfirmed, totalDeaths, totalRecovered;

            for (int i = 0; i < count; i++) {
                country = list.get(i).getCountry() != null ? list.get(i).getCountry() : "";
//                String appFormOrLosId = "";
//                if (list.get(i).getFormNumber().trim().length() > 0) {
//                    appFormOrLosId = list.get(i).getFormNumber();
//                } else {
//                    appFormOrLosId = list.get(i).getLosId();
//                }

               /* formNo = list.get(i).getFormNumber();
                losId = list.get(i).getLosId();*/
                //   contact = list.get(i).getSupervisorNumber();

                if (country.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }
            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            countriesCovidStatsList = (ArrayList<CountriesCovidStats>) results.values;
            notifyDataSetChanged();
        }
    }

}