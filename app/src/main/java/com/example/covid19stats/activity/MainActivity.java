package com.example.covid19stats.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.example.covid19stats.R;
import com.example.covid19stats.adapter.ListViewAdapter;
import com.example.covid19stats.databinding.ActivityMainBinding;
import com.example.covid19stats.model.CountriesCovidStats;
import com.example.covid19stats.model.CovidStats;
import com.example.covid19stats.resources.GPSTracker;
import com.example.covid19stats.resources.ResourceManager;
import com.example.covid19stats.rest.ApiClient;
import com.example.covid19stats.rest.ApiInterface;
import com.example.covid19stats.storage.AppPreferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final int REQUEST_LOCATION_PERMISSION_SETTING = 103;
    private static final int LOCATION_PHONE_STATE_CONSTANT = 104;
    private static String COUNTRY = "";
    private ActivityMainBinding binding;
    private List<CountriesCovidStats> countriesCovidStatsList;
    private ListViewAdapter listViewAdapter;
    private boolean sentToSettings = false;
    private AppPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        preferences = new AppPreferences(this);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Toast.makeText(TabParentActivity.this, "GPS is Enabled in your device", Toast.LENGTH_SHORT).show();
            locationPermission();
        } else {
            showGPSDisabledAlertToUser();
        }


        binding.countrySearchView.setQueryHint(getString(R.string.search_hint));
        binding.countrySearchView.setOnQueryTextListener(this);


        final Handler handler = new Handler();

        getCovidStats();
        new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 2 * 60 * 1000); // every 2 minutes
                getCovidStats();
            }
        }.run();


    }

    private void getCovidStats() {

        final AlertDialog alertProgressDialog = ResourceManager.getDialogProgressBar(MainActivity.this).create();
        alertProgressDialog.setCancelable(false);
        alertProgressDialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CovidStats> call = apiService.getCovidStats();

        call.enqueue(new Callback<CovidStats>() {
            @Override
            public void onResponse(Call<CovidStats> call, Response<CovidStats> response) {

                if (response.isSuccessful()) {

                    CovidStats covidStats = response.body();


                    onSuccessResponseReceive(covidStats);
                } else {
                    Toast.makeText(getBaseContext(), response.message(), Toast.LENGTH_LONG).show();

                }
                if (alertProgressDialog.isShowing() && alertProgressDialog != null) {
                    alertProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CovidStats> call, Throwable t) {
                if (alertProgressDialog.isShowing() && alertProgressDialog != null) {
                    alertProgressDialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), R.string.unable_connect_server, Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void onSuccessResponseReceive(CovidStats covidStats) {


        List<CountriesCovidStats> countriesCovidStatsList = new ArrayList<>();


        int index = 0;
        int totalConfirmed = 0, totalDeaths = 0, totalRecovered = 0;
        for (int i = 0; i < covidStats.getCountriesCovidStats().size(); i++) {
            totalConfirmed = totalConfirmed + covidStats.getCountriesCovidStats().get(i).getTotalConfirmed();
            totalDeaths = totalDeaths + covidStats.getCountriesCovidStats().get(i).getTotalDeaths();
            totalRecovered = totalRecovered + covidStats.getCountriesCovidStats().get(i).getTotalRecovered();

            if (covidStats.getCountriesCovidStats().get(i).getTotalConfirmed() > 0) {
                CountriesCovidStats countriesCovidStats = new CountriesCovidStats();
                countriesCovidStats.setCountry(covidStats.getCountriesCovidStats().get(i).getCountry());
                countriesCovidStats.setTotalConfirmed(covidStats.getCountriesCovidStats().get(i).getTotalConfirmed());
                countriesCovidStats.setTotalDeaths(covidStats.getCountriesCovidStats().get(i).getTotalDeaths());
                countriesCovidStats.setTotalRecovered(covidStats.getCountriesCovidStats().get(i).getTotalRecovered());
                countriesCovidStatsList.add(countriesCovidStats);
            }

            double latitude = 0, longitude = 0;

            if (preferences.getLatitude() != null && preferences.getLatitude().length() > 0) {
                latitude = Double.valueOf(preferences.getLatitude());
            }

            if (preferences.getLongitude() != null && preferences.getLongitude().length() > 0) {
                longitude = Double.valueOf(preferences.getLongitude());
            }

            if (latitude != 0 && longitude != 0) {
                try {
                    Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                    List<Address> addresses = null;
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    COUNTRY = addresses.get(0).getCountryName();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            if (countriesCovidStatsList.get(i).getCountry().equalsIgnoreCase(COUNTRY)) {

                binding.listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                binding.listView.setSelector(android.R.color.darker_gray);

            }

        }

        Collections.sort(countriesCovidStatsList, new Comparator<CountriesCovidStats>() {
            public int compare(CountriesCovidStats t1, CountriesCovidStats t2) {
                if (t1.getTotalConfirmed() >= t2.getTotalConfirmed()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

        listViewAdapter = new ListViewAdapter(this, countriesCovidStatsList);
        binding.listView.setAdapter(listViewAdapter);

        View v = binding.listView.getChildAt(0);
        int top = (v == null) ? 0 : v.getTop();
        binding.listView.setSelectionFromTop(index, top);
        listViewAdapter.notifyDataSetChanged();

        binding.totalConfirmedTextView.setText("Total Confirmed" + "\n" + String.valueOf(totalConfirmed));
        binding.totalDeathsTextView.setText("Total Deaths" + "\n" + String.valueOf(totalDeaths));
        binding.recoveredTextView.setText("Total Recovered" + "\n" + String.valueOf(totalRecovered));



    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listViewAdapter.getFilter().filter(newText);
        return false;
    }

    private void locationPermission() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((android.app.Activity) this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                /*Show Information about why you need the permission*/
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle(R.string.string_need_location_permission);
                builder.setMessage(R.string.app_need_location);
                builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PHONE_STATE_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (preferences.getFineLocationPermission()) {
                    /*Previously Permission Request was cancelled with 'Dont Ask Again',
                     Redirect to Settings after showing Information about why you need the permission*/
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle(R.string.string_need_location_permission);
                builder.setMessage(R.string.app_need_location);
                builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_LOCATION_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Location", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                /*just request the permission*/
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PHONE_STATE_CONSTANT);
            }
            preferences.setFineLocationPermission(true);
        } else {
            getTrackerLocation();
        }
    }

    private void showGPSDisabledAlertToUser() {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void getTrackerLocation() {

        GPSTracker gpsTracker = new GPSTracker(MainActivity.this);
        // Check if GPS enabled
        if (gpsTracker.canGetLocation()) {

            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            if (latitude != 0 && longitude != 0) {

                preferences.setLatitude(String.valueOf(latitude));
                preferences.setLongitude(String.valueOf(longitude));

            } else {
                preferences.setLatitude("0.00");
                preferences.setLongitude("0.00");

                locationPermission();
            }
        }
    }


}
