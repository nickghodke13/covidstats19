package com.example.covid19stats.rest;


import com.example.covid19stats.model.CovidStats;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("/summary")
    Call<CovidStats> getCovidStats();

}
